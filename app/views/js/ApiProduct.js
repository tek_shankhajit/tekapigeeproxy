   



var APIProduct = angular.module("APIProductapp", ['checklist-model','ngAnimate', 'ui.bootstrap']);

	  APIProduct.controller("APIProductController", function($scope,$http,$timeout,$uibModal,$rootScope){
	  
	  $scope.init = function(){
		  $http.defaults.headers.common['Authorization'] = 'Basic dmtvbnRoYW1AdGVrc3lzdGVtcy5jb206S29udGhhbSMxOTg4';
		  $scope.show = 'product';
			  	  		
			//Code to get the list of products and some details//
			
		  $http({
			  url:'https://api.enterprise.apigee.com/v1/organizations/vkontham/apiproducts',
			  method:'GET'
		  }).then(function(response){
			 $scope.productDetailList = response.data; 	
			 console.log($scope.productDetailList);
		  });	
		  $timeout( function(){ $scope.watch(); }, 400);
		  //$scope.watch();
	  }
	  
	  $scope.watch = function() { 
           document.getElementById("loader").style.display = "none";
           document.getElementById("myDivPage").style.display = "flex";
        };  
	  
$scope.init();


// To get product details from the above response on click on single product on UI

$scope.getAPIProduct = function(product){
		  var selectedProduct = product;
		  $scope.product = selectedProduct;
		  //console.log(selectedProduct);
		  $scope.show = 'showDetails';
		 
		  
		  $http.defaults.headers.common['Authorization'] = 'Basic dmtvbnRoYW1AdGVrc3lzdGVtcy5jb206S29udGhhbSMxOTg4';

		  $http({
			  url:'https://api.enterprise.apigee.com/v1/organizations/vkontham/apiproducts/' + selectedProduct,
			  method:'GET'
		  }).then(function(response){
			 $scope.selectedProduct = response.data; 
			// $scope.response = response;
			 //console.log($scope.response);
			 //var responseJSON= JSON.stringify($scope.response);
			 //console.log(responseJSON);
			 //console.log($scope.selectedProduct);
			
			 $scope.apiResources = $scope.selectedProduct.apiResources;
			 $scope.proxies = $scope.selectedProduct.proxies;
			 $scope.attributes = $scope.selectedProduct.attributes;
			 console.log($scope.apiResources);			 
			 console.log($scope.attributes);		
			 $http({
					url:'https://api.enterprise.apigee.com/v1/organizations/vkontham/apis',
					method:'GET'
				}).then(function(response){
					$scope.proxyList = response.data;
					console.log($scope.proxyList);
					console.log($scope.proxies);

					console.log($scope.proxyList);
					$scope.ResourceRemove=[];
					$scope.removeProxy=[];
					
									});
		
		
											 
			 });
				 
		  
  };  
  
	  // To enable the adding paths functionality
	  $scope.addPaths = function(){
		  
		  
		   // $scope.checkDisabled= "true";
			document.getElementById("fire1").style.visibility = 'visible';
			document.getElementById("AddButton").style.visibility = 'visible';
			
			};
			
			 $scope.addProxy= function(){
					
					document.getElementById("fire").style.visibility = 'visible';
					document.getElementById("addProxyButton").style.visibility = 'visible';
						   
				};
				
			
			$scope.enableRemove=function(){
				
				console.log($scope.ResourceRemove);
							
				if($scope.ResourceRemove.length >=1){
					 isChecked=true;
					document.getElementById("removeButton").style.visibility = 'visible';
					//return  isChecked ;
				}else{
					//return false;
					document.getElementById("removeButton").style.visibility = 'hidden'
				}
				
					
			};
			
			$scope.enableProxyRemove= function(){
				
				console.log($scope.removeProxy);
				if($scope.removeProxy.length >=1){
					 isChecked=true;
					document.getElementById("removeProxyButton").style.visibility = 'visible';
					//return  isChecked ;
				}else{
					//return false;
					document.getElementById("removeProxyButton").style.visibility = 'hidden';
				}
				
				
			};
			
           $scope.removePath =function(){
				
        	   //$scope.apiResources=$scope.ResourceRemove;
        	   console.log("length of array is ");
        	   console.log($scope.ResourceRemove.length);
        	   
        	   //var resourceRemove;
        		   for (var i =0; i<$scope.ResourceRemove.length;i++){
        			   
        			   var selected = $scope.ResourceRemove[i];
        			   var removal1 = $scope.ResourceRemove.indexOf(selected);
        			   console.log(selected);
        			   var removal= $scope.apiResources.indexOf(selected);
        			   console.log(removal);
        			   $scope.apiResources.splice(removal,1);
        			   //resourceRemove[i] = $scope.ResourceRemove[i];
        			   $scope.ResourceRemove.splice(removal1,1);
        			   
        			  
        			  // $scope.selectedProduct.apiResources= $scope.apiResources;
        		   }
        		   
        		   var length = $scope.apiResources.length;
        		   if(length==0){
    				   document.getElementById("removeButton").style.visibility = 'hidden';
    				   
    			   }
        		   
        		   
        		   console.log(resourceRemove);
        		   $scope.editAttributes = false;
					$scope.toJson(resourceRemove,$scope.editAttributes);        		   
        	   
        	   
			};
			
			$scope.removeProxyResources = function(){
				
				console.log("length of array is");
				console.log($scope.removeProxy.length);
				
					var length = $scope.removeProxy.length;
					
					for (var i =0;i<length;i++){
						
						var selected= $scope.removeProxy[i];
						var removal1= $scope.removeProxy.indexOf(selected);
						console.log(selected);
						var removal = $scope.proxies.indexOf(selected);
						console.log(selected);
						$scope.proxies.splice(removal,1);
						$scope.removeProxy.splice(removal1,1);
						console.log($scope.removeProxy);
												
					}
					
					var length = $scope.proxies.length;
					if(length==0){
						
						document.getElementById("removeProxyButton").style.visibility = 'hidden';
						
					}
					
					var removeProxy = $scope.removeProxy;
					$scope.editAttributes = false;
					$scope.toJson(removeProxy,$scope.editAttributes);
					
				
			};
			
			$scope.addPathsResources=function(){
				
				
				 $scope.animationsEnabled = true;
				 //$scope.apiResPath=[];
				 var modalInstance = $uibModal.open({
					 animation: $scope.animationsEnabled,
				      templateUrl: 'addPaths.html',
				      controller: 'ModalInstanceCtrl',
				      size:'sm',
//				      resolve: {
//				          params: function () {
//				            return $scope.apiResPath;
//				          }
//				        }
					 
				 });
				 	
				 modalInstance.result.then(function (apiPath) {
				     // $scope.selected = selectedItem;
					 console.log(apiPath);
					 $scope.path= apiPath[0];
					$scope.apiResources.push($scope.path);
					$scope.editAttributes = false;
					$scope.toJson(apiPath,$scope.editAttributes);
					 
				    }, function () {
				      console.log("dilog dismissed");
				    });
				 
			};
			
			$scope.addProxyResources = function(){
				
				for (var i=0;i<=$scope.proxyList.length;i++){
					
					for (var j=0;j<=$scope.proxies.length;j++){
						
						if($scope.proxies[j]==$scope.proxyList[i]){
							$scope.proxyList.splice(i,1);
							//console.log($scope.proxyList);
						}
						
					}
				}
				var $ctrl = this;
				$ctrl.proxyList = $scope.proxyList;
				$scope.animationsEnabled = true;
				
				var modalInstance = $uibModal.open({
					 animation: $scope.animationsEnabled,
				      templateUrl: 'addProxy.html',
				      controller: 'ModalInstanceCtrlProxy',
				      controllerAs: '$ctrl',
				      size:'sm',
				      resolve: {
				    	  proxies: function () {
				        	 return $ctrl.proxyList;
				            }
				        }
					 
				 });
				
				 modalInstance.result.then(function (apiProxy) {
				     
					 console.log(apiProxy);
					 $scope.proxy= apiProxy[0];
					$scope.proxies.push($scope.proxy);
					$scope.editAttributes = false;
					$scope.toJson(apiProxy,$scope.editAttributes);
					 
				    }, function () {
				      console.log("dilog dismissed");
				    });
				
				 
				
			};
			
			$scope.editField = function(){
				
				var $ctrl = this;
				$ctrl.attributes = $scope.attributes;
				$scope.animationsEnabled = true;
				
				var modalInstance = $uibModal.open({
					 animation: $scope.animationsEnabled,
				      templateUrl: 'editResources.html',
				      controller: 'ModalInstanceCtrlAttrib',
				      controllerAs: '$ctrl',
				      size:'sm',
				      resolve: {
				    	  attributes: function () {
				        	 return $ctrl.attributes;
				            }
				        }
					 
				 });
				
				 modalInstance.result.then(function (attributes) {
				     
					 console.log(attributes);
					 $scope.attributes = attributes;
					 $scope.editAttributes = true;
					 $scope.toJson(attributes,$scope.editAttributes);
				    }, function () {
				      console.log("dilog dismissed");
				    });
				
			};
			
			
$scope.toJson = function(apiResource,editAttributes){
	
	
	
	if (apiResource.length>0){
		console.log(apiResource);
		$scope.selectedProduct.apiResources = $scope.apiResources;
		var toJson= true;		
	}
	
	if(apiResource.length>0){
		console.log(apiResource);
		$scope.selectedProduct.proxies=$scope.proxies;
		var toJson = true;	
		$scope.count=1;
	}
	
	if(editAttributes){
		
		var productJSON = angular.toJson($scope.selectedProduct);
		console.log(productJSON);
		$scope.count=1;
		
	}
	
	if(toJson){
		
		$scope.count=1;
		
		$scope.productJSON = angular.toJson($scope.selectedProduct);
		console.log($scope.productJSON);
		
		
	}
};
		

$scope.updateAPIResourcedata = function(){
	
	var product = $scope.product;
	console.log(product);	
	if($scope.count==1){
				
	  $http({
		
		method:'put',
		url:'https://api.enterprise.apigee.com/v1/organizations/vkontham/apiproducts/'+product,
		data:$scope.productJSON,
	    headers : {
	        'Content-Type' : 'application/json'
	    }
//		data:'$scope.productJSON'
		
	}).success(function(response){
		
		alert("Product is Updated");
		
		
	}).error(function(response){
		
		$scope.errorData= response.data;
		
		alert("Error Encountered while updating the product:"+ $scope.errorData);
		
	});
	
	}
	else{
		
		alert("No updates to Save");
	}
	
};

	  });
  
	  APIProduct.controller("ModalInstanceCtrl",function($scope, $uibModalInstance,$rootScope){
	  
	  $scope.cancel = function(){
	  console.log("cancelmodal");
	  $uibModalInstance.dismiss('close');
	  }
	  $scope.addPathValue = function(Path){
		  $scope.pathArray =[];
		  console.log(Path);
		  $scope.pathArray.push(Path);
		  console.log($scope.pathArray);
		 //$scope.apiResources.push(Path);
		// $scope.selectedProduct.apiResources= $scope.apiResources;
		  $uibModalInstance.close($scope.pathArray);
	  
	  };
	  
	 });
	  
	  
	  
	  APIProduct.controller("ModalInstanceCtrlProxy",function($scope, $uibModalInstance,proxies){
		  
		  var $ctrl = this;
		  $ctrl.proxies = proxies;
		  $scope.proxies= $ctrl.proxies;
		  console.log($scope.proxies);
		  
		  $scope.cancel = function(){
			  console.log("cancelmodal");
			  $uibModalInstance.dismiss('close');
			  };
		  
			  $scope.addProxy = function(selectedProxy){
				  $scope.proxyArray =[];
				  console.log(selectedProxy);
				  $scope.proxyArray.push(selectedProxy);
				  console.log($scope.proxyArray);
				 //$scope.apiResources.push(Path);
				// $scope.selectedProduct.apiResources= $scope.apiResources;
				  $uibModalInstance.close($scope.proxyArray);
			  
			  };		  
		  
	  });
	  
APIProduct.controller("ModalInstanceCtrlAttrib",function($scope, $uibModalInstance,attributes){
		  
		  var $ctrl = this;
		  $ctrl.attributes = attributes;
		  $scope.attributes= $ctrl.attributes;
		  console.log($scope.attributes);
		  console.log($scope.attributes[0].name);
		  $scope.attr=$scope.attributes;
		  
		  $scope.cancel = function(){
			  console.log("cancelmodal");
			  $uibModalInstance.dismiss('close');
			  };
		  
			  $scope.saveAttributes = function(){
				  
				  for (var i=0;i<$scope.attributes.length;i++){
					  
					  $scope.attributes[i].name= $scope.attr[i].name;
					  $scope.attributes[i].value=$scope.attr[i].value;
					  
				  }
				  console.log($scope.attributes);
				  $uibModalInstance.close($scope.attributes);
			  
			  };		  
		  
	  });
	  
	  
	  