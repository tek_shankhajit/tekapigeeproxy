 var APIDevApp = angular.module("APIDev", ['checklist-model','ngAnimate', 'ui.bootstrap']);

  APIDevApp.controller("APIDevAppController", function($scope,$http,$timeout,$uibModal,$rootScope){

	  $scope.init = function(){
		  $http.defaults.headers.common['Authorization'] = 'Basic dmtvbnRoYW1AdGVrc3lzdGVtcy5jb206S29udGhhbSMxOTg4'
			  $scope.appDetailList =[];
			$scope.show = 'devapp';
			$scope.showConsumerKey=true;
			$scope.showConsumerSecret=true;
	  
		  //Code to get the list of Apps and some details//
		  $http({
			  url:'https://api.enterprise.apigee.com/v1/organizations/vkontham/apps?expand=true',
			  method:'GET'
		  }).then(function(response){
			 $scope.appDetailList = response.data;

			//console.log($scope.appDetailList);
			// console.log($scope.appDetailList.app);
		  		  
             console.log( $scope.appDetailList);
//             $scope.JsonString = angular.toJson($scope.appDetailList);
//             console.log($scope.JsonString);
			 console.log($scope.appDetailList.app[0].name);
		  	
			

	  });

		  $timeout( function(){ $scope.watch(); }, 400);
	  }
	  
	   $scope.watch = function() { 
			
           document.getElementById("loader").style.display = "none";
           document.getElementById("myDivPage").style.display = "flex";
        };  
	  
$scope.init();


// To get devApp details from the above response on click on s single dev App on UI

$scope.getAPIDevApp = function(devApp){
		  var selecteddevApp = devApp;
		  //console.log(selecteddevApp);
		 console.log(selecteddevApp);
		 $scope.selecteddevApp = selecteddevApp;
		  $scope.show = 'showAppDetails';
		  //$scope.productDetailList=[];
		  
		  $http.defaults.headers.common['Authorization'] = 'Basic dmtvbnRoYW1AdGVrc3lzdGVtcy5jb206S29udGhhbSMxOTg4';
		  $http({
			  url:'https://api.enterprise.apigee.com/v1/organizations/vkontham/apiproducts',
			  method:'GET'
		  }).then(function(response){
			  console.log(response.data);
			  
			 $scope.productDetailList = response.data; 	
			 console.log($scope.productDetailList);
			
		  });
		  
		 // console.log($scope.productDetailList);
		  angular.forEach($scope.appDetailList.app , function(appDetail) {
			 // console.log(appDetail.name);
			 
			  if(appDetail.name == selecteddevApp) {
				$scope.appDetail = appDetail;
				  $scope.JsonString = angular.toJson($scope.appDetail);
		             console.log($scope.JsonString);
				console.log(appDetail);
				$scope.attributes = appDetail.attributes;
				console.log($scope.attributes);
				$scope.showConsumerKey = true;
				$scope.showConsumerSecret= true;
					
				 $scope.creden = appDetail.credentials[0].apiProducts;	
				 console.log($scope.creden);
				  
			} 
			  

		});		  
		  
			  };
	  
			  $scope.showConsumerKeyValue = function(){
				  console.log("clicked");
					
				  if($scope.showConsumerKey){
					  $scope.showConsumerKey=false;
					  
				  }else{
					  $scope.showConsumerKey=true;
					  
					  //$scope.getAPIDevApp(devApp);
				  }
				  
				  
			  };
			  
			  $scope.showConsumerSecretValue = function(){
				 
				  if($scope.showConsumerSecret){
					  
					  $scope.showConsumerSecret=false;
					 
				  }else{
					  $scope.showConsumerSecret=true;
					
						  
				  }
				  
				  
			  };
	
$scope.editField = function(){
	
	var $ctrl = this;
	$ctrl.attributes = $scope.attributes;
	$scope.animationsEnabled = true;
	
	var modalInstance = $uibModal.open({
		 animation: $scope.animationsEnabled,
	      templateUrl: 'editResources.html',
	      controller: 'ModalInstanceCtrlAttrib',
	      controllerAs: '$ctrl',
	      size:'sm',
	      resolve: {
	    	  attributes: function () {
	        	 return $ctrl.attributes;
	            }
	        }
		 
	 });
	
	 modalInstance.result.then(function (attributes) {
	     
		 console.log(attributes);
		 $scope.attributes = attributes;
		 //$scope.editAttributes = true;
		 //$scope.toJson(attributes,$scope.editAttributes);
		 $scope.addtoAttributes($scope.attributes);
	    }, function () {
	      console.log("dilog dismissed");
	    });
	
};

$scope.addtoAttributes = function(attributes){
	
	var length = attributes.length;
	console.log(attributes);
	//$scope.attributes = attributes;
	$scope.appDetail.attributes = attributes;
	//console.log($scope.appDetail);
	console.log($scope.appDetailList.app);
	var toJson= true;
	$scope.toJSON($scope.appDetailList.app,toJson);
	
};



$scope.addProducts = function(){
	
	$scope.productArray =[];
	  
	  for (var i=0;i<$scope.creden.length;i++){
		  
		  $scope.productArray[i]=$scope.creden[i].apiproduct;
	  }
	  
	  //console.log($scope.productDetailList);
	  console.log($scope.productArray);
	
	console.log($scope.productDetailList);
	
	
	for (var i=0;i<=$scope.productDetailList.length;i++){
		
		
		 
		for (var j=0;j<$scope.productArray.length;j++){
			
			
			
			if($scope.productArray[j] == $scope.productDetailList[i]){
				
				

				$scope.productDetailList.splice(i,1);
				i--;
				
				
		}

		}
	}	
	
console.log($scope.productDetailList);
//
		var $ctrl = this;
		$ctrl.productDetailList = $scope.productDetailList;
		$ctrl.productArray = $scope.productArray;
		$scope.animationsEnabled = true;
		
		var modalInstance = $uibModal.open({
			 animation: $scope.animationsEnabled,
		      templateUrl: 'addProduct.html',
		      controller: 'ModalInstanceCtrlProduct',
		      controllerAs: '$ctrl',
		      size:'sm',
		      resolve: {
		    	  products: function () {
		        	 return $ctrl.productDetailList;
		        	 
		            },
		            productsDel: function () {
			        	 return $ctrl.productArray;
			        	 
			            }		
		        }
			 
		 });
		
		 modalInstance.result.then(function (apiProduct) {
		     
			// console.log(apiProduct);
			 $scope.apiProduct= apiProduct;
			 console.log($scope.apiProduct);
			 $scope.formProduct($scope.apiProduct);
			 
			//$scope.proxies.push($scope.proxy);
			//$scope.editAttributes = false;
			//$scope.toJson(apiProxy,$scope.editAttributes);
			 
		    }, function () {
		      console.log("dilog dismissed");
		    });


  
  };
  
  $scope.formProduct = function(apiProduct){
		
		var length = apiProduct.length;
		if (length>0){
			
			var i = Math.floor(Math.random() * 90 + 10);
			console.log(i);
			
			
			$scope.producttoAdd = {
					
					"apiproduct":apiProduct[0], "status": "approved", "$$hashKey": "object:"+i
							}
			console.log($scope.producttoAdd);
			$scope.addProduct($scope.producttoAdd);
		}
		
	};
	
	$scope.addProduct = function(producttoAdd){
		
		if(producttoAdd){
			console.log("inside addProduct function");
			
			var length = $scope.creden.length;
			$scope.creden.push(producttoAdd);
			
			$scope.appDetail.credentials[0].apiProducts = $scope.creden;
			console.log($scope.appDetail.credentials[0].apiProducts);
			console.log($scope.appDetailList.app);
			var toJson= true;
			$scope.toJSON($scope.appDetailList.app,toJson);

			
		}
		
	};
	
	$scope.toJSON = function(deatilList,toJson){
		
		if(toJson){
			//$scope.app =[]
			$scope.appList = deatilList;
			//$scope.appList= angular.toJson(deatilList);
			//console.log($scope.appList);
			$scope.app = {app:$scope.appList};
			$scope.appList = angular.toJson($scope.app);
			console.log($scope.appList);
			$scope.count =1;
			
		}
		
	}
	
	$scope.updateAPIResourcedata = function(){
		
		var selectedapp= $scope.selecteddevApp;
		var devEmail = "helloworld@apigee.com";
		if($scope.count ==1){
			
			 $http({
					
					method:'put',
					url:'https://api.enterprise.apigee.com/v1/organizations/vkontham/developers/'+devEmail+'/apps/'+selectedapp,
					data:$scope.appList,
				    headers : {
				        'Content-Type' : 'application/json'
				    }

					
				}).success(function(response){
					
					alert("App is Updated");
					
					
				}).error(function(response){
					
					$scope.errorData= response.data;
					
					alert("Error Encountered while updating the product:"+ $scope.errorData);
					
				});
				
				}
				else{
					
					alert("No updates to Save");
				}
				
		};
		
		
	
  });
  
  APIDevApp.controller("ModalInstanceCtrlAttrib",function($scope, $uibModalInstance,attributes){
	  
	  var $ctrl = this;
	  $ctrl.attributes = attributes;
	  $scope.attributes= $ctrl.attributes;
	  //console.log($scope.attributes);
	  //console.log($scope.attributes[0].name);
	  $scope.attr=$scope.attributes;
	  
	  $scope.cancel = function(){
		  console.log("cancelmodal");
		  $uibModalInstance.dismiss('close');
		  };
	  
		  $scope.saveAttributes = function(){
			  //$scope.attribArray ={};
			  for (var i=0;i<$scope.attributes.length;i++){
				  
				  $scope.attributes[i].name= $scope.attr[i].name;
				  $scope.attributes[i].value=$scope.attr[i].value;
				  
			  }
			  console.log($scope.attributes);
			  //$scope.proxyArray.push(selectedProxy);
			 // console.log($scope.proxyArray);
			 //$scope.apiResources.push(Path);
			// $scope.selectedProduct.apiResources= $scope.apiResources;
			  $uibModalInstance.close($scope.attributes);
		  
		  };		  
	  
  });
  
  APIDevApp.controller("ModalInstanceCtrlProduct",function($scope, $uibModalInstance,products,productsDel){
	  
	  var $ctrl = this;
	  $ctrl.products = products;
	  $ctrl.productsDel = productsDel;
	  $scope.products= $ctrl.products;
	  $scope.productsDel = $ctrl.productsDel;
	  console.log($scope.products);
	  console.log( $scope.productsDel);
	  
	  $scope.cancel = function(){
		  console.log("cancelmodal");
		  $uibModalInstance.dismiss('close');
		  };
	  
		  $scope.addProduct = function(selectedProduct){
			  $scope.productArray =[];
			  console.log(selectedProduct);
			  $scope.productArray.push(selectedProduct);
			  console.log($scope.productArray);
			 
			  $uibModalInstance.close($scope.productArray);
		  
		  };	
		  
		  $scope.delProduct = function(selectedProduct){
			  $scope.productArray =[];
			  console.log(selectedProduct);
			  $scope.productArray.push(selectedProduct);
			  console.log($scope.productArray);
			 
			  $uibModalInstance.close($scope.productArray);
		  
		  };
	  
  });