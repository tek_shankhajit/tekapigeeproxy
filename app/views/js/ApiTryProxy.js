 var APITryProxy = angular.module("APITryProxyApp", []);

  APITryProxy.controller("APITryProxyController", function($scope,$http,$location,$timeout){

	  $scope.init = function(){
		  var selectedProxy = $location.search().name; 
		  $http({
			  url:'http://HSC-PG017HYH.allegisgroup.com:4200/try',
			  method:'POST',
			  data: {proxyName: selectedProxy},
			  headers: { 'Content-Type': 'application/json' }
		  }).then(function(response){
			$scope.testTry = response.data;
			console.log($scope.testTry);
		  });
		  $timeout( function(){ $scope.watch(); }, 400);
	  }	
$scope.watch = function() { 
           document.getElementById("loader").style.display = "none";
           document.getElementById("myDivPage").style.display = "flex";
        }; 	  
$scope.init(); 

$scope.onepValueChange = function(epValue){
		  var selectedepValue = epValue;
		  console.log(selectedepValue);
		  var index = $scope.testTry.item.findIndex( item => item.request.url.raw === selectedepValue );
			console.log(index);
			if ($scope.testTry.item[index].request.body.raw != null) {
				console.log("in if block");
				$scope.testRequest = JSON.parse($scope.testTry.item[index].request.body.raw);
				$scope.testRequestData = JSON.stringify($scope.testRequest, undefined, 2);
			}
			if ($scope.testTry.item[index].request.header != null) {
				console.log("in if block");
				$scope.testRequestHeaders = $scope.testTry.item[index].request.header
			}
			$scope.testRequestMethod = $scope.testTry.item[index].request.method;
			$scope.show = 'requestDetails';
	  } 

$scope.sendRequest = function(epValue,testRequestData,testRequestHeaders,testRequestMethod ){
	var endpoint = epValue;
	var requestBody = testRequestData;
	var requestheader = angular.toJson(testRequestHeaders);
	var requestMethod = testRequestMethod;
	$http({
			  url:'http://HSC-PG017HYH.allegisgroup.com:4200/tryTest',
			  method:'POST',
			  data: {targetEndpoint: endpoint, reqBody:requestBody, reqHeader: requestheader, reqMethod: requestMethod},
			  headers: { 'Content-Type': 'application/json' }
		  }).then(function(response){
			$scope.testTry = response.data;
			console.log($scope.testTry);
			$scope.show = 'requestResponse';
		  });

	  } 
  });