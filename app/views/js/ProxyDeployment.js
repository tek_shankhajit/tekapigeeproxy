  var ProxiAPI = angular.module("ProxiAPIApp", []);

  ProxiAPI.controller("ProxiAPIController", function($scope,$http,$location,$timeout){
	  $scope.proxyDetailList;
	  $scope.pDetailsTemp ='[';
	  $scope.productDetailsTemp ='[';

	  $scope.init = function(){
		  var selectedProxy = $location.search().name; 
		  $http.defaults.headers.common['Authorization'] = 'Basic dmtvbnRoYW1AdGVrc3lzdGVtcy5jb206S29udGhhbSMxOTg4';
		  $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8;';

		  $http({
			  url:'https://api.enterprise.apigee.com/v1/organizations/vkontham/apis/' + selectedProxy,
			  method:'GET'
		  }).then(function(response){
			 $scope.selectedProxyToDeploy = response.data; 
		  });
		  
		  var deployedurl = 'https://api.enterprise.apigee.com/v1/organizations/vkontham/apis/' + selectedProxy + '/deployments';
		  $http({
			  url:deployedurl,
			  method:'GET'
		  }).then(function(response){
			  $scope.proxyRevDetails = [];			  
			 $scope.proxydeploymentDetails = response.data; 
			 angular.forEach($scope.proxydeploymentDetails.environment, function(environment) {
				 var deployedRevurl = 'https://api.enterprise.apigee.com/v1/organizations/vkontham/apis/' + selectedProxy + '/revisions/'+environment.revision[0].name;
				 $http({
						url:deployedRevurl,
						method:'GET'
						}).then(function(response){
													$scope.proxyRevDetails.push(response.data);
												});
			});
		  });

		  $http({
			  url:'https://api.enterprise.apigee.com/v1/organizations/vkontham/environments',
			  method:'GET'
		  }).then(function(response){
			 $scope.envList = response.data;
		  });
		  
		  //Deployment History
		  var url = 'http://HSC-PG017HYH.allegisgroup.com:3000/deployHistory?proxyname=' + selectedProxy ;
		  $http({
			  url:url,
			  method:'GET'
		  }).then(function(response){
			$scope.depHistory = response.data;
		  });
		  
        var selectedEnvdropDown = document.getElementById("selectedEnv");
        selectedEnvdropDown.selectedIndex = 0;
		var selectRevisiondropDown = document.getElementById("selectRevision");
        selectRevisiondropDown.selectedIndex = 0;
		$timeout( function(){ $scope.watch(); }, 500);
		//$scope.watch();
	  }
	  
	  $scope.watch = function() { 
           document.getElementById("loader").style.display = "none";
           document.getElementById("myDivPage").style.display = "flex";
        };  
$scope.init();

// Proxy deployment	
	  $scope.deployProxy = function(proxy,revision,environment,userName,jiraStory){
		  	var selectedProxy = proxy;		
			var selectedRevision = revision;
			var selectedEnvironment = environment;
			var selecteduserName = userName;
			var selectedjiraStory = jiraStory;
		   //$http.defaults.headers.common['Authorization'] = 'Basic dmtvbnRoYW1AdGVrc3lzdGVtcy5jb206S29udGhhbSMxOTg4';
		   $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8;';
			var url = 'https://api.enterprise.apigee.com/v1/organizations/vkontham/environments/'+selectedEnvironment+'/apis/'+selectedProxy+'/revisions/' + selectedRevision+'/deployments?override=true';
			var reqURL = encodeURI(url);
		  $http({
			  url:reqURL,
			  method:'POST',
			  data: '',
			  headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;', 'Access-Control-Allow-Headers' :'Origin, X-Requested-With, Content-Type, Accept, Authorization' }
		  }).then(function(response){
			if(response.status == 200){
				$scope.msg = "Proxy Successfully Deployed";
				$scope.show='depResponse';
				$http({
			  url:'http://HSC-PG017HYH.allegisgroup.com:3000/deploy',
			  method:'POST',
			  data: {proxy: selectedProxy, revision: selectedRevision, environment: selectedEnvironment, deployer: selecteduserName, jira: selectedjiraStory},
			  headers: { 'Content-Type': 'application/json'}
						}).then(function(response){
								console.log(response.data);	
					});
			}
			else{
				$scope.msg = "Proxy deployment failed, please contact Administrator";
				$scope.show='depResponse';
			}
		  });
	  }
	  
	  $scope.envFunction = function() {
	var selectENV = document.getElementById('selectedEnv');
    $scope.EnvToDeploy = selectENV.value;
}
	  $scope.revFunction = function() {
	var selectREV = document.getElementById('selectRevision');
    $scope.RevToDeploy = selectREV.value;
}

$scope.reload = function() {
    window.opener.location.reload(true);
}
	  });