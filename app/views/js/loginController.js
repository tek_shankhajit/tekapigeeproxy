/**
 * 
 */
  var appLogin = angular.module("loginApp", []);

  appLogin.controller("loginController", function($scope,$http,$q){
	  
	  	  
	$scope.verifyLogin = function(accID,password) {
		        
			var accountID = $scope.accID;		
			var pass = $scope.password;
			var url = 'http://HSC-PG017HYH.allegisgroup.com:3000/verify?accID=' + accountID + '&password=' + pass;
			
			   $http({
						url:url,
						method:'GET' 
					}).then(function(response){
						// console.log(response);
						
						$scope.verficationTemp = angular.toJson(response.data);
						$scope.verfication = $scope.verficationTemp.replace(/@/g, "");
						$scope.verfication = JSON.parse($scope.verfication);
						
						var redirect = $scope.verfication[1][0].valid; 
						// console.log('redirect:'+redirect);
						if(redirect == 0){
						sessionStorage.setItem('username',accountID);	
						window.location ="../html/home.html";	
						
						
						}
						else{
							window.location ="../html/login.html";
							var message = $scope.verfication[1][0].statusmessage;
							alert(message);
							
				                  
						}
					}).catch(function(response){
						
						//console.log(response);
						var responsedata = response.data;
						if(responsedata == null){
							
							alert("Service is unavailable. Please contact Administrator.")
						}						
						
						
					});
            }
			
	$scope.register = function(regiaccID,reginame,regipass) {
               
			var accountID = $scope.regiaccID;
			var name = $scope.reginame;			
			var pass = $scope.regipass;
			   
			   $http({
						url:'http://HSC-PG017HYH.allegisgroup.com:3000/register',
						method:'POST',
						data: {accID: accountID, name: name, pass: pass},
			            headers: { 'Content-Type': 'application/json' }
					}).then(function(response){
				 		$scope.registerTemp = angular.toJson(response.data);
						$scope.register = $scope.registerTemp.replace(/@/g, "");
						$scope.register = JSON.parse($scope.register);
						
						var message = $scope.register[1][0].statusmessage
						
						if(message == "Success"){
							
							alert("user registration successful");
							window.location = "../html/login.html";
						}else{
							
							alert("User registration failed. Please contact Administrator.")
						}
					}).catch(function(response){
						
						
						var responsedata = response.data;
						if(responsedata == null){
							
							alert("Service is unavailable. Please contact Administrator.")
						}						
						
					});
            }
  }); 
  
 
  