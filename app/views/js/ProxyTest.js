  var ProxiTest = angular.module("ProxiTestApp", []);

  ProxiTest.controller("ProxiTestController", function($scope,$http,$location){
	  
	  $scope.init = function(){
		  var selectedProxy = $location.search().name;
		  $http({
			  url:'http://HSC-PG017HYH.allegisgroup.com:4200/test',
			  method:'POST',
			  data: {proxyName: selectedProxy},
			  headers: { 'Content-Type': 'application/json' }
		  }).then(function(response){
			$scope.testTemp = angular.toJson(response.data);
			 $scope.testResponse = JSON.parse($scope.testTemp);	 
			 $scope.testCollection ={ Collection: $scope.testResponse.collection.info.name, ResponseTimeAverage: $scope.testResponse.run.timings.responseAverage, DataReceived: $scope.testResponse.run.transfers.responseTotal};
			 var testcaselist = {};
			 var key = 'TestCases';
			 testcaselist[key] = [];
			 var assesrtionsList = {};
			 angular.forEach($scope.testResponse.run.executions , function(executions) {
				var assertionlist = {};
				var key1 = 'Assertion';
				assertionlist[key1] = [];
				 angular.forEach(executions.assertions , function(assertions) {
					if (assertions.error == null ){
						$scope.testcaseAssertionsTemp = {Name: assertions.assertion, Message: "OK"} ;
					}
					else{
						$scope.testcaseAssertionsTemp = {Name: assertions.assertion, Message:assertions.error.name};
					}
				assertionlist[key1].push($scope.testcaseAssertionsTemp);
			 }); 
				var testEndpoint= executions.request.url.protocol + '://';
				angular.forEach(executions.request.url.host , function(host) {
				testEndpoint = testEndpoint + host + '.';
				});
				testEndpoint = testEndpoint.substring(0,testEndpoint.length-1); 
				angular.forEach(executions.request.url.path , function(path) {
				testEndpoint = testEndpoint+ '/' + path;
				});
				var queryParam = '';
				angular.forEach(executions.request.url.query , function(query) {
				queryParam = queryParam+ query.key+ '=' +query.value+ '&';
				});
				//queryParam = queryParam.substring(0,queryParam.length-1);
					if (queryParam != ''){
						queryParam = queryParam.substring(0,queryParam.length-1);
						testEndpoint = testEndpoint + '?' + queryParam
					}
				$scope.testcaseTemp =  {TestCase: executions.item.name,UrlEndpoint:executions.request.method + ' ' + testEndpoint,ResponseStatus: executions.response.code + ' ' + executions.response.status,ResponseTime:executions.response.responseTime, assertionlist};
				testcaselist[key].push($scope.testcaseTemp);
			 });
			 $scope.collectionTestCaseResults = testcaselist;
			 $scope.watch();
		  });
	  }
$scope.init();

$scope.watch = function() { 
                document.getElementById("loader").style.display = "none";
  document.getElementById("myDivPage").style.display = "flex";
        };    
});
